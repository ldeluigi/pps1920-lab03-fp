package u02

import org.junit.jupiter.api.Test
import org.junit.jupiter.api.Assertions._

import u03.Lists.List._
import u02.Optionals.Option


class ListTest {

  val l = Cons(10, Cons(20, Cons(30, Nil())))
  val lst = Cons(10, Cons(20, Cons(30, Nil())))
  val lst2 = Cons(3, Cons(7, Cons(1, Cons(5, Nil()))))

  @Test def testSum(): Unit = {
    assertEquals(60, sum(l))
  }

  @Test def testAppend(): Unit = {
    assertEquals(Cons(5, Cons(10, Cons(20, Cons(30, Nil())))), append(Cons(5, Nil()), l))
  }

  @Test def testDrop() {
    assertEquals(Cons(20, Cons(30, Nil())), drop(lst, 1))
    assertEquals(Cons(30, Nil()), drop(lst, 2))
    assertEquals(Nil(), drop(lst, 5))
  }

  @Test def testFlatMap() {
    assertEquals(Cons(11, Cons(21, Cons(31, Nil()))), flatMap(lst)(v => Cons(v + 1, Nil())))
    assertEquals(Cons(11, Cons(12, Cons(21, Cons(22, Cons(31, Cons(32, Nil())))))), flatMap(lst)(v => Cons(v + 1, Cons(v + 2, Nil()))))
  }

  @Test def testMap(): Unit = {
    assertEquals(Cons(20, Cons(40, Cons(60, Nil()))), map(lst)(e => e * 2))
  }

  @Test def testFilter(): Unit = {
    assertEquals(Cons(30, Nil()), filter(lst)(e => e > 25))
    assertEquals(Cons(20, Cons(30, Nil())), filter[Int](l)(_ >= 20))
  }

  @Test def testMax(): Unit = {
    assertEquals(Option.Some(25), max(Cons(10, Cons(25, Cons(20, Nil())))))
    assertEquals(Option.None(), max(Nil()))
  }

  @Test def testFoldLeft(): Unit = {
    assertEquals(-16, foldLeft(lst2)(0)(_ - _))
    assertEquals(0, foldLeft(Nil[Int]())(0)(_ - _))
  }

  @Test def testFoldRight() {
    assertEquals(-8, foldRight(lst2)(0)(_ - _))
    assertEquals(0, foldRight(Nil[Int]())(0)(_ - _))
  }
}
