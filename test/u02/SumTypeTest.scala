package u02

import org.junit.jupiter.api.Test
import org.junit.jupiter.api.Assertions._

class SumTypeTest {

  import SumTypes._
  import u03.Lists.List
  import u03.Lists.List._

  val l: List[Person] = Cons(Teacher("mario", "analisi"), Cons(Student("fabio", 2020), Cons(Teacher("nadia", "mdp"), Nil())));

  @Test def testName() {
    assertEquals("mario",
      name(Student("mario", 2015)))
  }

  @Test def testSum(): Unit = {
    assertEquals(60,
      SumTypes.sum(ILCons(10, ILCons(20, ILCons(30, ILNil())))))
  }

  @Test def testCourses(): Unit = {
    assertEquals(Cons("analisi", Cons("mdp", Nil())), courses(l))
  }
}
