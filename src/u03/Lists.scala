package u03

import scala.annotation.tailrec

object Lists {

  import u02.Optionals.Option
  import u02.Optionals.Option._

  // A generic linkedlist
  sealed trait List[E]

  // a companion object (i.e., module) for List
  object List {

    case class Cons[E](head: E, tail: List[E]) extends List[E]

    case class Nil[E]() extends List[E]

    def sum(l: List[Int]): Int = l match {
      case Cons(h, t) => h + sum(t)
      case _ => 0
    }

    def append[A](l1: List[A], l2: List[A]): List[A] = (l1, l2) match {
      case (Cons(h, t), l2) => Cons(h, append(t, l2))
      case _ => l2
    }

    def drop[A](l: List[A], n: Int): List[A] = (l, n) match {
      case (Cons(_, t), x) if x > 0 => drop(t, x - 1)
      case _ => l
    }

    def max(l: List[Int]): Option[Int] = {
      @tailrec def _max(n: Option[Int], l: List[Int]): Option[Int] = (n, l) match {
        case (Some(x), Cons(h, t)) => _max(Some(Math.max(h, x)), t)
        case (None(), Cons(h, t)) => _max(Some(h), t)
        case _ => n
      }

      _max(None(), l)
    }

    def flatMap[A, B](l: List[A])(f: A => List[B]): List[B] = l match {
      case Cons(h, t) => append(f(h), flatMap(t)(f))
      case _ => Nil()
    }

    def map[A, B](l: List[A])(mapper: A => B): List[B] = flatMap(l)(m => Cons(mapper(m), Nil()))

    def filter[A](l: List[A])(pred: A => Boolean): List[A] = flatMap(l)(e => if (pred(e)) Cons(e, Nil()) else Nil())

    def foldRight[A, B](l: List[A])(seed: B)(binaryOperator: (A, B) => B): B = l match {
      case Cons(h, t) => binaryOperator(h, foldRight(t) (seed) (binaryOperator))
      case _ => seed
    }

    @tailrec
    def foldLeft[A, B](l: List[A])(seed: B)(binaryOperator: (B, A) => B): B = l match {
      case Cons(h, t) => foldLeft(t)(binaryOperator(seed, h))(binaryOperator)
      case _ => seed
    }

    /*def foldRight[A, B](l: List[A])(seed: B)(accumulator: (A, B) => B): B = {
      @tailrec
      def _foldRight(l: List[A], seed: B, acc: (A, B) => B, curr: B => B): B = l match {
        case Cons(x, tail) => _foldRight(tail, seed, acc, e => curr(acc(x, e)))
        case _ => curr(seed)
      }
      _foldRight(l, seed, accumulator, e => e)
    }*/

  }
}
