package u02

import u03.Lists._

object SumTypes extends App {
  import u03.Lists.List._


  // Sum type: a sealed base trait, and cases extending it
  sealed trait Person //sealed: no other impl. except Student, Teacher
  case class Student(name: String, year: Int) extends Person
  case class Teacher(name: String, course: String) extends Person

  def name(p: Person): String = p match {
    case Student(n,_) => n
    case Teacher(n,_) => n
  }

  // A LinkedList of Int
  sealed trait IntList
  case class ILCons(head: Int, tail: IntList) extends IntList
  case class ILNil() extends IntList

  def sum(l: IntList): Int = l match {
    case ILCons(h, t) => h + sum(t)
    case _ => 0
  }

  def courses(l: List[Person]): List[String] = flatMap(l)({
    case Teacher(_, c) => Cons(c, Nil())
    case _ => Nil()
  })
}
